# Samply Simple DAO

Samply Simple DAO is a *simple* data access object library. It offers simple classes that
simplify the access to a relational database, especially PostgreSQL. Is also offers a well
defined upgrade mechanism for the developer.

# Repository moved!

As of May 31st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.common.simpledao. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked samply.common.simpledao in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).
